-------------
Code
-------------
Im Ordner Praktikum 1/ befinden sich die 3 Unterordner: 

1.- fasterfrequentwords.py
2.- findingfrequentwordsbysorting.py
3.- frequentwords.py

In den jeweiligen Unterordnern befinden sich die entsprechenden Quellcodes.
Diese Codes wurden in Python geschrieben und daher unter Verwendung von JupyterNotebooks bearbeitet.


-------------------
500-Fenster Testen
-------------------
Die von uns benutzen 500-Fenster sind die folgenden:

1.- Thermotoga petrophila

AAACTCCAGAGTGGGTAGATAAAGAACACATCTCGCTGCTCCACGAAATGGGAGTTGTTTTGAGAAAGAAAAAGGGCATTCAACCTGCGCAGAATCCTATGGAACAGGCCTTTCTCACACTCAGAATAGGATACGATCAGTTCTTCGAAGAAGACTTTGACATGGAATCTTTTGTGAAAGATTTTATGGAAAAATTGAAAAGAATGTATGAGGTCCTCGTTTCAATGTTATAATAAATACCGTGCAAAAACAGTTGGACGAAGGTTCTGATCCCTACAGAACACCTGCCCTGAAATGGTCCCTCAGGAGAATCATCGAGGAGTTAACCGCTCAGGGCTCTTTTTGAAATCTCTCCTATCACTTCATCGATCAGGCTCTTCAGTTGTTTGTTCCCTTTGAGGAGGGAATCTTTGACTCTTTTGACACTGTCTAAAACAACAGGGTGTGACCTGTTGAATTTCTCCGCTATCGTTCTCAGAGAACTATTCAGATGGTTCTTGG

2.- Vibrio Cholerae

GTTTATATCCTACTGGCAATTAAATCATTAAAAGAAATAACCAGTAGTTATTAACAATAACTGATCAAAATCAAAAACATGGGGATAACTACTAAAGATCTTGAGATATGTGGATCTTTATGTGGGTAGCACGGGCAAAATGTGTGAGGATCTTAGTTATCGGTCGAAAAATAATGTGAATAACTTAGATCTTATTCACTGGATCGACGATCCAGCGCTGGCGATCTGAGTTATCAACAGGTAGAATTGCTCCTCTTTACCGATCGTTGATTTTTGAGTGAGGGAATCGTGTCATCTTCGCTATGGTTGCAATGTTTGCAACGGCTTCAGGAAGAGCTACCTGCCGCAGAATTCAGTATGTGGGTGCGTCCGCTTCAAGCGGAGCTCAATGACAATACTCTCACTTTATTCGCCCCGAACCGCTTTGTGTTGGATTGGGTACGCGATAAGTACCTCAATAACATCAATCGTCTGCTGATGGAATTCAGTGGCAATGATGTG


Diese können mit den 3 Codes im Ordner Praktikum 1/ 
getestet werden.
